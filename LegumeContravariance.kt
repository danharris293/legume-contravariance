/**
 * Legume
 * A leguminous plant (member of the pea family), especially one grown as a crop.
 */
open class Legume

/**
 * Chickpea
 * A round yellowish edible seed, widely used as a pulse.
 * A legume of the family Fabaceae, subfamily Faboideae.
 */
class Chickpea : Legume()

/**
 * Seller
 * Someone who sells things.
 */
class Seller<out T>

/**
 * Buyer
 * Someone who buys things.
 */
class Buyer<in T>

/**
 * Market
 * A place for sellers to sell and buyers to buy.
 */
data class Market(var legumeSeller: Seller<Legume>?, var chickpeaBuyer: Buyer<Chickpea>?)

fun theContravarianceOfLegumes() {

    /**
     * Suppose we have a Market that is looking for someone to sell Legumes.
     * The spot is currently vacant...
     */
    val market: Market = Market(legumeSeller = null, chickpeaBuyer = null)

    /**
     * Market doesn't care what Legumes the seller sells.
     * Market just needs to be able to say that it has someone who sells Legumes.
     * As You is a person that knows people - and people know that You is a person that knows people -
     * the Market goes to You:
     * Market: "Hey, You! Do you know any Legume sellers?"
     * You: "Why, yes of course! Here, let me introduce you to Chris... She sells Chickpeas."
     * *Chris stops awkwardly hovering nearby and comes over*
     */
    val chris: Seller<Chickpea> = Seller()

    /**
     * Market: "Who only sells Chickpeas?"
     * Chris: "I'm stood righ..."
     * You: "Chris does! You said that you were looking for someone who sells Legumes and Chris sells Chickpeas."
     * Chris: "And Chickpeas are Legu..."
     * You: "And Chickpeas are Legumes, so Chris sells Legumes."
     * Market: "Right. Well, do you know anyone that sells more than just Chickpeas?"
     * Chris: "Again. Stood righ..."
     * You: "Nope, just Chris!"
     * Market: "Okay, fine. Chris, you've got the job."
     * Chris: "Cool beans."
     * *You and Market share a look and are inwardly disgusted at Chris' unashamed use of a bean-based pun...*
     * *...but also slightly amused*
     */
    market.legumeSeller = chris

    /**
     * Chris immediately sets up shop so that she can peagin selling her Chickpeas.
     * .
     * .
     * .
     * Some time passes though and Market realises that no one wants to buy Chris' Chickpeas!
     * There are plenty of people wanting to buy butter beans and kidney beans but not Chickpeas.
     * Market decides to approach You again:
     * Market: "No one is buying Chris' Chickpeas!"
     * You: "Are you kidneying me?!"
     * *Market pukes in his mouth a little but quickly swallows it and moves on*
     * *You is also ashamed... but in a good way*
     * Market: "No, I'm not. I need you to find me someone who loves buying Chickpeas and send them my way."
     * You: "Not a problem. I know just your man. Sean, come on over here!"
     */
    val sean: Buyer<Legume> = Buyer()

    /**
     * Market: "Has he bean hiding behind that... Wait a minute. I know you! You're..."
     * Sean: "Sean Bean. Yes, I'm well aware. My surname is Bean, I'm famous and I buy Legumes. And don't think I
     *        didn't hear your bean pun just then, either. You, how can I help?"
     * You: "Well Market has this shoddy Chickpea seller named Chris..."
     * Chris: "Who is RIGH..."
     * You: "...but unsurprisingly, no one wants to buy her Chickpeas and as you love buying Legumes..."
     * Market: "Hang on a minute! Sean buys Legumes?! I need someone to buy Chris' Chickpeas, not Legumes."
     * *You and Sean are appalled at how Market struggles to understand the contravariance of the situation*
     * Sean: "Yes, and Chickpeas are Legumes..."
     * You: "...so Sean will buy Chris' Chickpeas!"
     * Market: "Hmm. So Sean can buy Chris' Chickpeas, and Chris can sell Sean herpes? I mean her peas!"
     * You: "They'll be like two peas in a pod!"
     * Sean: "Hummusing!"
     * *Sean had to go and take it too far*
     */
    market.chickpeaBuyer = sean
}
